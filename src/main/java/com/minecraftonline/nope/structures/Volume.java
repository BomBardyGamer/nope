/*
 * MIT License
 *
 * Copyright (c) 2021 MinecraftOnline
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.minecraftonline.nope.structures;

public interface Volume {

  /**
   * Get minimum X value, inclusive.
   *
   * @return x min
   */
  int getMinX();

  /**
   * Get maximum X value, inclusive.
   *
   * @return x max
   */
  int getMaxX();

  /**
   * Get minimum Y value, inclusive.
   *
   * @return y min
   */
  int getMinY();

  /**
   * Get maximum Y value, inclusive.
   *
   * @return y max
   */
  int getMaxY();

  /**
   * Get minimum Z value, inclusive.
   *
   * @return z min
   */
  int getMinZ();

  /**
   * Get maximum Z value, inclusive.
   *
   * @return z max
   */
  int getMaxZ();

  /**
   * Check if this volume contains a point, given three
   * cartesian coordinates.
   *
   * @param x x value
   * @param y y value
   * @param z z value
   * @return true if the point is contained
   */
  boolean contains(int x, int y, int z);

  /**
   * Check if this volume contains another volume
   *
   * @param other another volume
   * @return true if contains
   */
  boolean contains(Volume other);

  /**
   * Check if this volume contains a cuboid
   *
   * @param other a cuboid
   * @return true if contains
   */
  boolean contains(Cuboid other);

  /**
   * Check if this volume contains a sphere
   *
   * @param other a sphere
   * @return true if contains
   */
  boolean contains(Sphere other);

  /**
   * Check if this volume intersects with another volume
   * or shares a face.
   *
   * @param other another volume
   * @return true if intersects
   */
  boolean intersects(Volume other);

  /**
   * Check if this volume intersects with a cuboid
   * or shares a face.
   *
   * @param other a cuboid
   * @return true if intersects
   */
  boolean intersects(Cuboid other);

  /**
   * Check if this sphere intersects with a sphere
   *
   * @param other a sphere
   * @return true if intersects
   */
  boolean intersects(Sphere other);

}
