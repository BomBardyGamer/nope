package com.minecraftonline.nope.mixin.collision;

import net.minecraft.entity.EntityLivingBase;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EntityLivingBase.class)
public abstract class InvulnerableEntityLivingMixin {

    @Inject(method = "knockBack", at = @At("HEAD"), cancellable = true)
    public void onKnockBack(CallbackInfo ci) {
        // make all invulnerable entities immune to explosions
        if (((EntityLivingBase)(Object)this).getIsInvulnerable()) ci.cancel();
    }
}