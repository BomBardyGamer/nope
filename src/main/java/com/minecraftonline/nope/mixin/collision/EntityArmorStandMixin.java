package com.minecraftonline.nope.mixin.collision;

import com.minecraftonline.nope.Nope;
import com.minecraftonline.nope.setting.SettingLibrary;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.world.World;
import org.spongepowered.api.world.Location;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityArmorStand.class)
public abstract class EntityArmorStandMixin extends EntityLivingBase {
    public EntityArmorStandMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "isImmuneToExplosions", at = @At("HEAD"), cancellable = true)
    public void onIsImmuneToExplosions(CallbackInfoReturnable<Boolean> cir) {
        if(!Nope.getInstance().getHostTree().lookupAnonymous(SettingLibrary.ARMOR_STAND_EXPLOSIVE_DESTROY,
                new Location<>((org.spongepowered.api.world.World) this.world, this.posX, this.posY, this.posZ))) {
            cir.setReturnValue(true);
            // Check the setting, make it immune if not allowed to explode them. Essentially, a @DynamicSettingListener in functionality.
        }
    }
}